## SrPago Libraries
This repository contains the libraries used for card tokenization and encryption,
# SrPago Libraries

* In the srpago_js/src folder you can find the libraries that are currently both in Production and Sandbox enviroments.
* In the srpago_js/src/resource/ contains the libraries used to construct srpago.encryption.v1.js

## Getting Started

### Dependencies

* Node ^14

### Development process

* Clone the repo and install dependencies
```
npm run install
```

### Minify files

Generate minified files for the 4 libraries and save them the /minified directory.
```
npm run minify
```

## konfiopagos.v2.js Initialization

### Public Key
```
SrPago.setPublishableKey(<PUBLIC_KEY>);
```
### Environments

Embed the konfiopagos.v2.min.js in the client where card tokenization will take place.
You can read more about the tokenization flow on the [devcenter](https://devcenter.srpago.com/docs/librerias/tokenizacion/tokenizacion-javascript/).

Initialize the library with Sandbox environment by passing "false" to setLiveMode()
and "true" for Production:

```
SrPago.setLiveMode(false)
```

To initialize the library with Development or Staging, pass the environment prefix as a second parameter; "dev" for Development or "stg" for Staging:

```
SrPago.setLiveMode(false, "dev");
```

### Deploy

Note: CI desploys ONLY konfipagos.v2.min.js ; the other libraries are not yet automatically deployed as they are stable versions.

## Library Roadmap

- Development branch is a work in progress improvement of this libraries that introduces ES6 Modules + Webpack.

## Version History

* 0.2
    * jQuery free library and optimizations
* 0.1
    * Initial Release

## License

This project is licensed under the [NAME HERE] License - see the LICENSE.md file for details

## Acknowledgments

References
* [libraries-documentation](https://devcenter.srpago.com/docs/librerias/tokenizacion/tokenizacion-javascript/)