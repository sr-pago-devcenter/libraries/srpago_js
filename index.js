const UglifyJS = require("uglify-js");
const fs = require("fs");
const path = require('path');

var destinationFolder = './minified';

const directoryPath = path.join(__dirname, '/srpago_js/src/');
allFiles = []

fs.readdirSync(directoryPath).filter(file => {
  if(file.indexOf(".js")>-1) allFiles.push(file);
});

fs.rmSync(destinationFolder, { recursive: true, force: true });
fs.mkdirSync(destinationFolder);

allFiles.forEach(function (file) {
    console.log(directoryPath+file)
    fs.readFile( directoryPath + file, (error, data) => {
        if(error) {
            throw error;
        }
        code = data.toString();
        let result = UglifyJS.minify(code);
        fs.writeFileSync(destinationFolder+'/'+file.replace('.js', '.min.js'), result.code);
    });
    
});