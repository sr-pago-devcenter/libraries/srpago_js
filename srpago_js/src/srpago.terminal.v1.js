(function() {


  this.SrPago.terminal = function() {

    function terminal() {}

    terminal._params = {
      onEvent: function(evt) {},
      onError: function(evt) {},
      onPaymentError: function(evt) {},
      onPaymentStatusChange: function(evt) {}
    };

    terminal._last_event = {name:"",data:{}};
    terminal._selected = null;
    terminal._terminals = [];


    terminal.setup = function(params) {
            if(params){
                for(var x in params){
                    terminal._params[x] = params[x];
                }
            }
    }

    terminal.terminals = function(readers){
         if(typeof(readers) =="object"){
            terminal._terminals = readers;
         }
         return terminal._terminals;

    }
    terminal.terminalById = function(id){
            var result = null;
            var terminals = terminal._terminals;
           for (var i = 0; i < terminals.length; i++) {
              if (terminals[i].id == id) {
                result = terminals[i];
              }
            }
            return result;
    }

    terminal.disconnect = function(options) {
      terminal._selected = null;
    }

    terminal.display = function(data) {
      return terminal.request({
        endpoint: "/v1/display",
        method: "POST",
        data: data
      }, success, fail);
    }



    terminal.connect = function(ter, success, fail) {

      if(typeof ter == "string"){
        ter = terminal.terminalById(ter);
      }


      if (!(typeof success === "function")) {
        success = function(a) {

        };
      }
      if (!(typeof fail === "function")) {
        fail = function(a) {

        };
      }


      fsuccess = function(f) {
        terminal._selected = ter;
        success(f);
      }
      ffail = function(f) {
        terminal._selected = null;
        fail(f);
      }

      return terminal.request({
        endpoint: "/v1/connect",
        terminal: ter
      }, fsuccess, ffail);


    }


    terminal.disconnect = function(success, fail) {


      if (!(typeof success === "function")) {
        success = function(a) {};
      }
      if (!(typeof fail === "function")) {
        fail = function(a) {};
      }

      var r = terminal.request({
        endpoint: "/v1/disconnect"
      }, success, fail);

      terminal._selected = null;
        return r;
    }


    terminal.charge = function(data, success, fail) {
       terminal._last_event = {};
       if(!(data && data.token)){
            fail({status:false,error:{code:"invalid_param","message":"token is required"}});
       }

      var params = {
        endpoint: "/v1/charge",
        method: "POST",
        data: data
      }

      fsuccess = function(obj){
            success(obj);
            terminal._manageEvent(obj);
            terminal._listen();
      }

      return terminal.request(params, fsuccess, fail);
    }

    terminal._isEqual = function(obj1, obj2){
        return JSON.stringify(obj1) === JSON.stringify(obj2);
    }

    terminal._parse = function(e){
        var response = { success:false, error:{code:"error_response", message:"Invalid format response ", detail: e}};

        if(e){
            response.success = e.success === true;

            var o = e.result?e.result: (e.error?e.error:{});
            var o2 = {};

            if(o.code) o2.code = o.code.toLowerCase().replace("on_","");
            if(o.message) o2.message = o.message;
            if(o.detail) o2.detail = o.detail;



            response.error = null;response.result = null;
            if(response.success){
                response.result = o2;
            }else{
                response.error = o2;
            }

        }


        return response;
    }

    terminal._manageEvent = function(evt){
        var response = terminal._parse(evt) ;

        var camel = function(str) {
          return str.replace(/\W+(.)/g, function(match, chr) {
                return chr.toUpperCase();
          });
        }

        if(terminal._isEqual(terminal._last_event, evt)){
            return;
        }
        terminal._last_event = evt;

        if(response.success==true){
            terminal._params.onPaymentStatusChange(response);
            if(response.result && response.result.detail){
                if(response.result.detail.transaction){
                    terminal._stop();
                }
            }

        }else{
           terminal._params.onPaymentError(response);
            if(response.error.code =="payment_error"){
                //temp* isChip
                if(response.error.message && response.error.message.indexOf("tiene chip")>0){
                    return;
                }

                terminal._stop();
            }
        }



     }


    terminal._listen = function(){
        terminal._interval = setInterval(function(){
            terminal.status(terminal._manageEvent,terminal._manageEvent);
        }, 3000);

    }
    terminal._stop = function(){
        clearInterval(terminal._interval);
        terminal._last_event = {};
    }


    terminal.status = function( success, fail) {
      return terminal.request({
        endpoint: "/v1/status"
      }, success, fail);
    }

    terminal.cancel = function(token, success, fail) {
       terminal.request({
        endpoint: "/v1/cancel"
      }, success, fail);

      terrminal._stop();
    }

    terminal.formatError = function(response, status) {
        var error = {success:false,error:{
            code: status,
            message: typeof(response)=="string"?response:""
        }};

        if (response && response.responseJSON) {
            response = response.responseJSON;
            if (response.error) {
                error = response;
            }
        }
        if(response && response.error){
            error = response;
        }
        return error;
    };


    terminal.request = function(params, success, fail) {

      if (!(typeof success === "function")) {
        success = function(a) { };
      }
      if (!(typeof fail === "function")) {
        fail = function(a) { };
      }

      var url = "";

      if (terminal._selected) {
        ter = terminal._selected;
      } else if (params.terminal) {
        ter = params.terminal;
      } else {
        fail(terminal.formatError("terminal is required","invalid_param_terminal"));
        return;
      }

      if (ter.ip_address === null) {
        fail(terminal.formatError("ip_address is required","invalid_param_ip_address"));
        return;
      }



      var url = (ter.protocol != null ? ter.protocol : "http") + "://" + ter.ip_address +
        (ter.port != null ? (ter.port == "" ? "" : ":" + ter.port) : ":5960");


      var request = {
        url: url + params.endpoint,
        method: params.method?params.method:"GET",
        success: function(body, status) {
          if (body && body.success) {
            return success(body);
          }
          return fail(terminal.formatError(body,status));
        },
        error: function(body, status) {
            if(status == "error") status="network_error";
          return fail(terminal.formatError(body,status));
        },
        timeout: params.timeout === null ? 5000 : params.timeout
      };

      if (params.method == "POST" || params.method == "PUT") {
        if (params.data) {
          request.data = params.data;
        } else {
          request.data = {};
        }
      }

      if (params.data) {
        request.data = params.data;
      }

      return SrPago.request(request);
    }

    return terminal;

  }();


}).call(this);
