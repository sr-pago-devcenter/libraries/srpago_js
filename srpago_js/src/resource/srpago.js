/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
/* AES implementation in JavaScript                                                               */
/* https://github.com/ricmoo/aes-js                                                               */
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
/* RSA implementation in JavaScript                                                               */
/* https://npmcdn.com/jsencrypt                                                                   */
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */

function randomString(length, chars) {
    var mask = '';
    if (chars.indexOf('a') > -1)
        mask += 'abcdefghijklmnopqrstuvwxyz';
    if (chars.indexOf('A') > -1)
        mask += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    if (chars.indexOf('#') > -1)
        mask += '0123456789';
    if (chars.indexOf('!') > -1)
        mask += '~`!@#$%^&*()_+-={}[]:";\'<>?,./|\\';
    var result = '';
    for (var i = length; i > 0; --i)
        result += mask[Math.floor(Math.random() * mask.length)];
    return result;
}


function cryptoApi(plainData) {
    var publicKey = "-----BEGIN PUBLIC KEY-----\
            MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAv0utLFjwHQk+1aLjxl9t\
            Ojvt/qFD1HfMFzjYa4d3iFKrQtvxaWM/B/6ltPn6+Pez+dOd59zFmzNHg33h8S0p\
            aZ6wmNv3mwp4hCJttGzFvl2hhw8Z+OU9KwGSXgQ+5FNyRyDLp0qt75ayvV0vV8oX\
            0Pgubd/NTHzRKk0ubXO8WVWkNhMdsv0HGrhIMDXAWLAQBzDewmICVH9MIJzjoZym\
            R7AuNpefD4hoVK8cBMjZ0xRKSPyd3zI6uJyERcR3+N9nxvg4guShP27cnD9qpLt4\
            L6YtU0BU+husFXoHL6Y2CsxyzxT9mtorAGe5oRiTC7Z/S9u7pxGN4iozgmAei0MZ\
            VbKows/qa9/q0PPzbF/PHSZKou1DJvsJ2PKY3ZPYAT7/u4x8NRiJ/6cssuzsIPUd\
            Q9HBzA1ZBMHkpOmkipu1G7ks/GwTfQJkHPW5xHu1EOYvgv/PHr3BJnCMNYKFvf5c\
            4Qd0COnnU3jDel1OKl7lUzr+ioqUedX393D/fszdK4hjvtUjo6ThTRNm3y4avY/r\
            m+oLu8sZWpyBm4PfN2xGOnFco9SiyCT03XOEuOXokid6BDMi0aue9LKJaQR+KGVc\
            /H2p2d2Yu4GdgXS1vq1syaf7V0QPOmamTOyJRZ45UoLfBRB8nYBGDo0mPR7GIon6\
            M8SmGGsTo3V0L+Ni9bNJHa8CAwEAAQ==\
      -----END PUBLIC KEY-----";

    var plainKey = randomString(56, '#aA').substr(0, 32);
    var numberFill = 16-(plainData.length%16);
    while(numberFill>0){
        numberFill--;
        plainData+=' ';
    }
    var key = aesjs.util.convertStringToBytes(plainKey);
    var plainBytes = aesjs.util.convertStringToBytes(plainData);
    var aesEcb = new aesjs.ModeOfOperation.ecb(key);
    var encryptedBytes = aesEcb.encrypt(plainBytes);
    var dataBase64 = btoa(String.fromCharCode.apply(null, new Uint8Array(encryptedBytes)));

    var encrypt = new JSEncrypt();
    encrypt.setPublicKey(publicKey);
    var encryptedKey = encrypt.encrypt(plainKey);

    var rs = '{"key":"'+encryptedKey+'","data":"'+dataBase64+'"}';
    
    return rs;
}