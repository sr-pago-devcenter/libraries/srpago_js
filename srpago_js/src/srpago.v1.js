(function() {
    var SrPago, exports, isSafeSrPagoDomain, scripts, SrPagojsBaseUrl, __hasProp = {}.hasOwnProperty,
        __extends = function(child, parent) {
            for (var key in parent) {
                if (__hasProp.call(parent, key)) child[key] = parent[key];
            }

            function parent() {
                this.constructor = child;
            }

            parent.prototype = parent.prototype;
            child.prototype = new parent();
            child.__super__ = parent.prototype;
            return child;
        };

    SrPagojsBaseUrl = 'https://js.srpago.com';

    isSafeSrPagoDomain = /srpago\.com$/.test(typeof window !== "undefined" && window !== null ? window.location.host : void 0) ? true : false;


    if (!isSafeSrPagoDomain && 'querySelectorAll' in document && !false) {
        scripts = document.querySelectorAll('script[src^="' + SrPagojsBaseUrl + '"]');
        if (!scripts.length) {
            console.log('The srpago.js must be included from https://js.srpago.com');
        }
    }

    function InvalidParamException(message, description){
      this.message = message;
      this.description = description;
      this.toString = function() {
         return this.message;
      };

    }


    SrPago = (function() {
        SrPago._sdkVersion = '0.1.1';
        SrPago._apiVersion = "v1";
        SrPago._liveMode = false;
        SrPago._apiBase = 'https://api.srpago.com';
        SrPago._apiBaseSandbox = 'https://sandbox-api.srpago.com';

        function SrPago() {

        }


        SrPago.setPublishableKey = function(key) {
            SrPago._publishableKey = key;
        };
        SrPago.getPublishableKey = function() {
            return SrPago._publishableKey;
        };
        SrPago.getApiBase = function() {
            var url = SrPago._apiBase;
            if (SrPago._liveMode == false) {
                url = SrPago._apiBaseSandbox;
            }
            return url;
        };
        SrPago.setLiveMode = function(liveMode) {
            SrPago._liveMode = liveMode;
        };

        SrPago.getApiVersion = function() {
            return SrPago._apiVersion;
        };

        SrPago.getUserAgent = function(key) {
            return {
                "language": "javascript",
                "sdk_version": SrPago._sdkVersion,
                "api_version": SrPago._apiVersion
            };
        };

        SrPago._isSafeDomain = isSafeSrPagoDomain;

        SrPago.request = function(params) {
            params.crossDomain = true;
            params.beforeSend = function(request) {
                var userAgent = SrPago.getUserAgent();
                if (params.extra_params) {
                    userAgent.extra_params = params.extra_params;
                }


                request.setRequestHeader("X-User-Agent", JSON.stringify(userAgent));
                request.setRequestHeader("Content-Type", "application/json");
                request.setRequestHeader("Authorization", "Basic " + btoa(SrPago.getPublishableKey() + ':'));
            };

            if (params.data && (typeof params.data) !== "string") {
                params.data = JSON.stringify(params.data);
            }

            return $.ajax(params);
        };

        SrPago.reportError = function(error, timing) {
            if ('console' in window) {
                console.log(error);
            }
        };

        SrPago.formatError = function(response, status) {
            var error = {
                code: "CommunicationException",
                message: "Hubo un problema al establecer la conexión con Sr.Pago"
            };

            if (response && response.responseJSON) {
                response = response.responseJSON;
                if (response.error) {
                    error = response.error;
                }
            }else if(response && response.message){
              error = response.message;
            }
            return error;
        };

        return SrPago;

    })();


    if (this.SrPago) {
        if (!this.SrPago.isDoubleLoaded) {
            console.log('Al parecer SrPago.js fue cargado dos veces, favor de revisar con el administrador del sitio.');
        }
        this.SrPago.isDoubleLoaded = true;
        return;
    }

    this.SrPago = SrPago;
    this.SrPago.token = (function() {
        function token() {}

        token.validate = function(data, name) {
            if (!data) {
                throw name + ' required';
            }
            if (typeof data !== 'object') {
                throw name + ' invalid';
            }
        };

        token.formatData = function(data) {
            var k, v, _ref;

            if (!data) {
                data = {};
            }

            /*if(jQuery && data instanceof jQuery){
              data = data.serializeArray();
            }*/


            for (k in data) {
                v = data[k];
                if (v == null) {
                    delete data[k];
                }
            }

            expiration = SrPago.card.__getExpirationDate(data.exp_month, data.exp_year);
            data.expiration = expiration;

            return data;
        };


        token.create = function(params, success, fail) {
            if (!(typeof success === "function")) {
                success = function(a) {
                    console.log(a);
                };
            }
            if (!(typeof fail === "function")) {
                fail = function(a) {
                    console.log(a);
                };
            }

            try{
              params = SrPago.token.formatData(params);
            }catch(excepcion){
              if (excepcion instanceof InvalidParamException) {
                fail(SrPago.formatError(excepcion));
              }else {
                fail(SrPago.formatError());
              }
              return;
            }
            var extra_params = {
                encrypted: false
            };
            if (SrPago.encryption) {
                params = SrPago.encryption.encrypt(params);
                extra_params.encrypted = true;
            }

            var request = {
                url: "" + SrPago.getApiBase() + "/" + SrPago.getApiVersion() + "/token",
                data: params,
                method: 'POST',
                headers: {},
                extra_params: extra_params,
                success: function(body, status) {
                    if (body && body.result) {
                        return success(body.result);
                    }
                    return fail(SrPago.formatError(body));
                },
                error: function(body, status) {
                    return fail(SrPago.formatError(body));
                },
                timeout: 40000,
                tokenType: "card"
            };

            return SrPago.request(request);
        };

        return token;
    })();



    this.SrPago.card = (function() {
        function card() {}

        card.cardTypes = function() {
            var types = {34: "AMEX",37: "AMEX"};
            for (var i = 40; i <= 49; i++) types[i] = "VISA";
            for (var i = 50; i <= 59; i++) types[i] = "MAST";
            for (var i = 22; i <= 27; i++) types[i] = "MAST";
            return types;
        };
        card.cardType = function(number) {
            var cardTypes = card.cardTypes();
            return cardTypes[number.slice(0, 2)] || "InvalidCardType";
        };
        card.validateNumber  = card.validateCardNumber = function(number) {
            number = ("" + number).replace(/\s+|-/g, "");
            return number.length >= 15 && number.length <= 16 && card.luhnValidate(number);
        };


        card.validateExpiration = card.validateExpirationDate = function(month, year) {
            var result = true;
            try{
              card.__getExpirationDate(month, year);
            }catch(e){
              result = false;
            }
            return result;
        };

        card.__getExpirationDate = function(month, year) {
              var exp_year = year ? ("" + parseInt("0" + year)).slice(-2) : "";;
              var exp_month = month ? ("" + parseInt("0" + month)).slice(-2) : "";
              var expiration = exp_year + "" + (parseInt(exp_month) < 10 ? "0" : "") + parseInt(exp_month);

              var now = new Date();
              now.setMonth(now.getMonth() + 2);
              var year = (now.getYear() + "").slice(-2);
              var month = (now.getMonth() < 10 ? "0" : "") + now.getMonth();
              var min_expiration = year + month;

              if (!card.between(parseInt(exp_month), 1, 12)) {
                  throw new InvalidParamException({
                      code: "InvalidParamException",
                      detail: "exp_month",
                      message: "El mes es requerido, el mes debe ser entre 1 - 12"
                  });
              }

              if (!card.between(parseInt(exp_year), year, 40)) {
                  throw new InvalidParamException({
                      code: "InvalidParamException",
                      detail: "exp_year",
                      message: "El año es requerido, el año debe ser entre " + year + " - 40"
                  });
              }

              if (!card.between(parseInt(expiration), min_expiration, 3512)) {
                  throw new InvalidParamException({
                      code: "InvalidParamException",
                      detail: "exp_year",
                      message: "La fecha es inválida"
                  });
              }

              return expiration;
        };


        card.validateCVC = card.validateCVV = function(number) {
            number = ("" + number).trim();
            return b = /^\d+$/.test(number) && number.length >= 3 && number.length <= 4
        };
        card.luhnValidate = function(number) {
            var b, c, sum, e;
            for (sum = +number[b = number.length - 1], e = 0; b--;) {
                c = +number[b], sum += ++e % 2 ? 2 * c % 10 + (c > 4) : c;
            }
            return !(sum % 10);
        };

        card.between = function(n, min, max) {
            n = n === +n && n === (n | 0) ? n : 0;
            return n >= min && n <= max;
        };


        return card;
    })();

}).call(this);
